package repositoriesImp;

import java.sql.*;
import java.util.List;

import repositories.IEntityBuilder;
import repositories.IUserRepository;
import unitOfWork.IUnitOfWork;
import unitOfWork.IUnitOfWorkRepository;
import entities.Entity;
import entities.Role;
import entities.User;

public class UserRepository 
extends Repository<User> implements IUserRepository{

	public UserRepository(Connection connection, IEntityBuilder<User> builder, IUnitOfWork uow) {
		super(connection,builder, uow);
	}

	@Override
	protected String getTableName() {
		return "users";
	}

	@Override
	protected String getUpdateQuery() {
		return 
				"UPDATE users SET (login,password)=(?,?) WHERE id=?";
	}

	@Override
	protected String getInsertQuery() {
		return "INSERT INTO users(login,password)"
				+ "VALUES(?,?)";
	}


	@Override
	protected void setUpInsertQuery(User entity) throws SQLException {
		
		insert.setString(1, entity.getLogin());
		insert.setString(2, entity.getPassword());
		
	}

	@Override
	protected void setUpUpdateQuery(User entity) throws SQLException {
		update.setString(1, entity.getLogin());
		update.setString(2, entity.getPassword());
		update.setInt(3, entity.getId());
		
		
	}

	
	public List<User> withRole(Role role) {
		return null;
	}

	
	public List<User> withRole(String roleName) {
		return null;
	}

	
	public List<User> withRole(int roleId) {
		return null;
	}

	
	public void persistAdd(User entity) {
		
		try {
			setUpInsertQuery(entity);
			insert.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void persistUpdate(User entity) {
		
		try {
			setUpUpdateQuery(entity);
			update.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void persistDelete(User entity) {
		
		try {
			delete.setInt(1, entity.getId());
			delete.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	public void commit() {
		// TODO Auto-generated method stub
		
	}

	public void rollback() {
		// TODO Auto-generated method stub
		
	}

	public void markAsNew(Entity entity, IUnitOfWorkRepository repository) {
		// TODO Auto-generated method stub
		
	}

	public void markAsDirty(Entity entity, IUnitOfWorkRepository repository) {
		// TODO Auto-generated method stub
		
	}

	public void markAsDeleted(Entity entity, IUnitOfWorkRepository repository) {
		// TODO Auto-generated method stub
		
	}

}